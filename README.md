# HelloWorld

[![CI Status](http://img.shields.io/travis/Aleksander Zubala/HelloWorld.svg?style=flat)](https://travis-ci.org/Aleksander Zubala/HelloWorld)
[![Version](https://img.shields.io/cocoapods/v/HelloWorld.svg?style=flat)](http://cocoadocs.org/docsets/HelloWorld)
[![License](https://img.shields.io/cocoapods/l/HelloWorld.svg?style=flat)](http://cocoadocs.org/docsets/HelloWorld)
[![Platform](https://img.shields.io/cocoapods/p/HelloWorld.svg?style=flat)](http://cocoadocs.org/docsets/HelloWorld)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HelloWorld is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "HelloWorld"

## Author

Aleksander Zubala, azubala@applause.com

## License

HelloWorld is available under the MIT license. See the LICENSE file for more info.

