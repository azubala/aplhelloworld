//
//  APLHelloWorld.m
//  Pods
//
//  Created by Aleksander Zubala on 02/10/14.
//
//

#import "APLHelloWorld.h"

@implementation APLHelloWorld

- (void)doYourJob {
    NSLog(@"Hello World");
}

@end
