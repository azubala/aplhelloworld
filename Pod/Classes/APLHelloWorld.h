//
//  APLHelloWorld.h
//  Pods
//
//  Created by Aleksander Zubala on 02/10/14.
//
//

#import <Foundation/Foundation.h>

@interface APLHelloWorld : NSObject

- (void)doYourJob;

@end
