//
//  main.m
//  HelloWorld
//
//  Created by Aleksander Zubala on 10/02/2014.
//  Copyright (c) 2014 Aleksander Zubala. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APLAppDelegate class]));
    }
}
