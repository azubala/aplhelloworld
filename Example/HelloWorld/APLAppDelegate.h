//
//  APLAppDelegate.h
//  HelloWorld
//
//  Created by CocoaPods on 10/02/2014.
//  Copyright (c) 2014 Aleksander Zubala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
